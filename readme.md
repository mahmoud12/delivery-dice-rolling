# Instructions

Implement a set of classes based on the following interfaces that will execute the code below.  Make sure you add appropriate error handling, exception handling, logging, and assertions as appropriate.  Your implementation should reflect best practices, layers of abstraction, code reuse, good object-oriented principles, and suitable design patterns.


## Build Instructions
- Build the project    
`./composer.phar install`

- Run the project with the following command    
`./composer.phar run-script index`

- Run the tests with the following command    
`vendor/bin/phpunit`
