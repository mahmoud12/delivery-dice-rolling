<?php

namespace DeliveryDotCom\Models;

use DeliveryDotCom\Contracts\DiceContainerInterface;
use DeliveryDotCom\Contracts\DiceInterface;

/**
 * Class for die with 8 sides
 * Inherits from AnyDie
 */
class D8 extends D
{
    private const SIDES = 8;

    /**
     * Constructor for D8
     * Sets sides member variable to const SIDES
     */
    function __construct()
    {
        $this->sides = self::SIDES;
    }
}