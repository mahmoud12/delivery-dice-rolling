<?php
namespace DeliveryDotCom\Models;

use DeliveryDotCom\Contracts\DiceContainerInterface;
use DeliveryDotCom\Contracts\DiceInterface;

/**
 * Class for die with 6 sides
 * Inherits from AnyDie
 */
class D6 extends D
{
    private const SIDES = 6;

    /**
     * Constructor for D6
     * Sets sides member variable to const SIDES
     */
    function __construct()
    {
        $this->sides = self::SIDES;
    }
}