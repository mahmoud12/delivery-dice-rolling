<?php

namespace DeliveryDotCom\Models;

use DeliveryDotCom\Contracts\DiceContainerInterface;
use DeliveryDotCom\Contracts\DiceInterface;


/**
 * Class implements DiceContainerInterface
 *
 * @category
 * @author Mahmoud Dolah <mahmoudamindolah@gmail.com>
 * @link
 * @package
 */
class MyDice implements DiceContainerInterface
{
    private $_diceContainer = array();

    /**
     * Attaches Die to container which will be
     * rolled in getTotal function
     *
     * @param AnyDie (implements DiceInterface) $die
     * @return boolean
     */
    public function attach(DiceInterface $die)
    {
        try {
            array_push($this->_diceContainer, $die);
        }
        catch (\Exception $e) {
            echo 'Caught exception: ' . $e->getMessage(), "\n";
        }
    }

    /**
     * Rolls all the dice and returns the total
     *
     * @return int
     */
    public function getTotal()
    {
        $total = 0;
        foreach ($this->_diceContainer as $die) {
            $total += $die->roll();
        }
        return $total;
    }
}