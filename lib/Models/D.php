<?php
namespace DeliveryDotCom\Models;

use DeliveryDotCom\Models\AnyDie;
use DeliveryDotCom\Contracts\DiceContainerInterface;
use DeliveryDotCom\Contracts\DiceInterface;

/**
 * Class for die with any number of sides
 * Inherits from AnyDie
 */
class D implements DiceInterface
{
    /**
     * Constructor for any faced die place
     * Sets sides member variable to parameter
     *
     * @param int $sides
     */
    function __construct($sides)
    {
        $this->sides = $sides;
    }

    /**
     * Simulates a dice roll
     * Return a random number between 0 and total number of sides
     *
     * @return int
     */
    public function roll()
    {
        $rolled = 0;
        try {
            $rolled = rand(1, $this->sides);
        } catch (\Exception $e) {
            die("Could not generate dice roll");
        }
        return $rolled;
    }
}