<?php
namespace DeliveryDotCom\Models;

use DeliveryDotCom\Contracts\DiceContainerInterface;
use DeliveryDotCom\Contracts\DiceInterface;
use TheSeer\Tokenizer\Exception;

/**
 * Class for die with 4 sides
 * Inherits from AnyDie
 */
class D4 extends D
{
    private const SIDES = 4;

    /**
     * Constructor for D4
     * Sets sides member variable to const SIDES
     */
    function __construct()
    {
        $this->sides = self::SIDES;
    }
}