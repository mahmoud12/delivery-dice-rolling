<?php
namespace DeliveryDotCom\Models;

use DeliveryDotCom\Contracts\DiceContainerInterface;
use DeliveryDotCom\Contracts\DiceInterface;

/**
 * Class for dice with any number of sides
 */
class AnyDie implements Diceinterface
{
    protected $sides;
    private $_dice = array();
    /**
     * Constructor for AnyDie
     * Takes an array of integers
     *
     * @param array $dice Array of integers that represent
     * the sides of an individual die
     */
    function __construct($dice)
    {
        foreach ($dice as $die) {
            array_push($this->_dice, new D($die));
        }
    }

    /**
     * Simulates a dice roll
     * Return a random number between 0 and total number of sides
     *
     * @return int
     */
    public function roll()
    {
        $rolled = 0;
        foreach ($this->_dice as $die) {
            $rolled += $die->roll();
        }
        return $rolled;
    }

}
