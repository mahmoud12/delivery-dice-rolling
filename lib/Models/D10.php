<?php
namespace DeliveryDotCom\Models;

use DeliveryDotCom\Contracts\DiceContainerInterface;
use DeliveryDotCom\Contracts\DiceInterface;

/**
 * Class for die with 10 sides
 * Inherits from AnyDie
 */
class D10 extends D
{
    private const SIDES = 10;

    /**
     * Constructor for D10
     * Sets sides member variable to const SIDES
     */
    function __construct()
    {
        $this->sides = self::SIDES;
    }
}