<?php

namespace DeliveryDotCom\Test;


use \PHPUnit\Framework\TestCase;
use DeliveryDotCom\Models\MyDice;
use DeliveryDotCom\Models\D10;
use DeliveryDotCom\Models\D8;
use DeliveryDotCom\Models\D6;
use DeliveryDotCom\Models\D4;

/**
 * Class to test MyDice
 */
class MyDiceTest extends TestCase
{
    /**
     * Test that total of dice roll is in proper range
     *
     * @return void
     */
    function testTotalInRange()
    {
        $mydie = new MyDice();
        $mydie->attach(new D10());
        $mydie->attach(new D8());
        $mydie->attach(new D6());
        $mydie->attach(new D4());
        $this->assertEquals(
            \filter_var(
                $mydie->getTotal(),
                FILTER_VALIDATE_INT,
                array(
                    'options' => array(
                        'min_range' => 1,
                        'max_range' => 28
                        )
                    )
            ),
            true
        );
    }

}
