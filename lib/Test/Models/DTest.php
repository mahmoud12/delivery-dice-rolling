<?php

namespace DeliveryDotCom\Test;

// require_once __DIR__ . '/../../vendor/autoload.php';

use \PHPUnit\Framework\TestCase;
use DeliveryDotCom\Models\MyDice;
use DeliveryDotCom\Models\D10;
use DeliveryDotCom\Models\D8;
use DeliveryDotCom\Models\D6;
use DeliveryDotCom\Models\D4;
use DeliveryDotCom\Models\D;

/**
 * Class to test individual die
 */
class DTest extends TestCase
{
    private const MINROLL = 1;
    private const MINSIDES = 2;

    /**
     * Test Dice roll to ensure it's within the proper range
     *
     * @dataProvider providerTestRoll
     *
     * @param DiceInterface $die Die to test roll method on
     * @param int $max Largest number that can be rolled
     *
     * @return void
     */
    function testRoll($die, $max)
    {
        $this->assertEquals(
            \filter_var(
                $die->roll(),
                FILTER_VALIDATE_INT,
                array(
                    'options' => array(
                        'min_range' => self::MINROLL,
                        'max_range' => $max
                        )
                    )
            ),
            true
        );
    }

    /**
     * Provide test data for testRoll()
     *
     * @return void
     */
    public function providerTestRoll()
    {
        $anySides = rand(0, 120);
        return array(
            array(new D4(), 4),
            array(new D6(), 6),
            array(new D8(), 8),
            array(new D10, 10),
            array(new D($anySides), $anySides)
        );
    }
}